package com.soaint.apiSalvavidas.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "CIUDAD_MUNICIPIO")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class CiudadMunicipio extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "CIUDAD_MUNICIPIO_GENERATOR")
    @Column(name = "ID_CIUD_MUN")
    @SequenceGenerator(
            name = "CIUDAD_MUNICIPIO_GENERATOR",
            sequenceName = "CIUDAD_MUNICIPIO_SEQ",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "CODIGO",nullable = false, length = 10)
    private String codigo;

    @Column(name = "DESCRIPCION",nullable = false, length = 100)
    private String descripcion;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_DEPARTAMENTO", nullable = false,foreignKey = @ForeignKey(name="CIU_MUN_DEPARTAMENTO_FK"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Departamento departamento;


}
