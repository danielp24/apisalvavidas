package com.soaint.apiSalvavidas.model.entities;

import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "PERSONA")
@EqualsAndHashCode(callSuper = false, of = {"id","numeroIdentificacion"})
public class Persona extends BaseEntity implements Serializable {


    @Id
    @GeneratedValue(generator = "PERSONA_GENERATOR")
    @Column(name = "ID_PERSONA")
    @SequenceGenerator(
            name = "PERSONA_GENERATOR",
            sequenceName = "PERSONA_SEQ",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "ID_PERFIL")
    private Long idPerfil;

    @Column(name = "NOMBRE1",nullable = false, length = 100)
    private String nombre1;

    @Column(name = "NOMBRE2",nullable = false, length = 100)
    private String nombre2;

    @Column(name = "APELLIDO1",nullable = false, length = 100)
    private String apellido1;

    @Column(name = "APELLIDO2",nullable = false, length = 100)
    private String apellido2;

    @Column(name = "ID_IDENTIFICACION",nullable = false)
    private Long idIdentificacion;

    @Column(name = "NUMERO_IDENTIFICACION",nullable = false)
    private Long numeroIdentificacion;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "ID_GENERO")
    private Long idGenero;

    @Column(name = "ID_EDO_CIVIL")
    private Long idEdoCivil;

    @Column(name = "ID_REGIMEN")
    private Long idRegimen;

    @Column(name = "DESC_REGIMEN", length = 250)
    private String descRegimen;

    @Column(name = "ID_PER_REP")
    private Long idPerRep;

    @OneToMany(mappedBy="persona",cascade=CascadeType.ALL)
    private List<DatosDemograficos> datosDemograficos = new ArrayList<>();

    @OneToOne(mappedBy = "persona", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private Contacto contacto = new Contacto();

    @OneToOne(mappedBy = "persona", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private DatosAcceso datosAcceso = new DatosAcceso();




}
