package com.soaint.apiSalvavidas.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "DATOS_DEMOGRAFICOS")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class DatosDemograficos extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "DATOS_DEMOGRAFICOS_GENERATOR")
    @Column(name = "ID_DEMOGRAFICO")
    @SequenceGenerator(
            name = "DATOS_DEMOGRAFICOS_GENERATOR",
            sequenceName = "DATOS_DEMOGRAFICOS_SEQ",
            allocationSize = 1
    )
    private Long id;


    @Column(name = "ID_PAIS", nullable = false)
    private Integer codPais;

    @Column(name = "ID_DEPARTAMENTO", nullable = false)
    private Integer codDepartamento;

    @Column(name = "ID_CIUD_MUN", nullable = false)
    private Integer codMunicipio;

    @Column(name = "LOCALIDAD", length = 50)
    private String localidad;

    @Column(name = "BARRIO", nullable = false,length = 100)
    private String barrio;

    @Column(name = "DIRECCION", nullable = false,length = 250)
    private String direccion;

    //Foreing Keys

    @ManyToOne(fetch = FetchType.LAZY,cascade= CascadeType.ALL)
    @JoinColumn(name = "ID_PERSONA")
    private Persona persona;


}
