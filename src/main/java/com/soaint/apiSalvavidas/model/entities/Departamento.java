package com.soaint.apiSalvavidas.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "DEPARTAMENTO")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class Departamento extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "DEPARTAMENTO_GENERATOR")
    @Column(name = "ID_DEPARTAMENTO")
    @SequenceGenerator(
            name = "DEPARTAMENTO_GENERATOR",
            sequenceName = "DEPARTAMENTO_SEQ",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "CODIGO",nullable = false, length = 10)
    private String codigo;

    @Column(name = "DESCRIPCION",nullable = false, length = 100)
    private String descripcion;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_PAIS", nullable = false,foreignKey = @ForeignKey(name="DEPARTAMENTO_PAIS_FK"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Pais pais;


}
