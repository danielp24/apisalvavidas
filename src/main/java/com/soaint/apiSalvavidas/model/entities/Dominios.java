package com.soaint.apiSalvavidas.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "DOMINIOS")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class Dominios extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "DOMINIOS_GENERATOR")
    @Column(name = "ID_DOMINIOS")
    @SequenceGenerator(
            name = "DOMINIOS_GENERATOR",
            sequenceName = "DOMINIOS_SEQ",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "CODIGO",nullable = false, length = 10)
    private String codigo;

    @Column(name = "DESCRIPCION",nullable = false, length = 250)
    private String descripcion;

    @Column(name = "COD_PADRE", length = 10)
    private String codPadre;
}
