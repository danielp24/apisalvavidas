package com.soaint.apiSalvavidas.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "CONTACTO")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class Contacto extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "CONTACTO_GENERATOR")
    @Column(name = "ID_CONTACTO")
    @SequenceGenerator(
            name = "CONTACTO_GENERATOR",
            sequenceName = "CONTACTO_SEQ",
            allocationSize = 1
    )
    private Long id;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID_PERSONA",foreignKey = @ForeignKey(name="CONTACTO_PERSONA_FK"))
    private Persona persona;

    @Column(name = "EMAIL",length = 250)
    private String email;

    @Column(name = "ID_TIPO_TLF_1")
    private Long idTipoTlf1;

    @Column(name = "NUMERO_TLF_1", length = 50)
    private String numeroTlf1;

    @Column(name = "ID_TIPO_TLF_2")
    private Long idTipoTlf2;

    @Column(name = "NUMERO_TLF_2",length = 50)
    private String numeroTlf2;

}
