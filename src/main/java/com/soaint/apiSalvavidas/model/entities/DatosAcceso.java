package com.soaint.apiSalvavidas.model.entities;

import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "DATOS_ACCESO")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class DatosAcceso extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "DATOS_ACCESO_GENERATOR")
    @Column(name = "ID_ACCESO")
    @SequenceGenerator(
            name = "DATOS_ACCESO_GENERATOR",
            sequenceName = "DATOS_ACCESO_SEQ",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "CLAVE",nullable = false, length = 200)
    private String clave;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID_PERSONA",foreignKey = @ForeignKey(name="DATOSACCESO_PERSONA_FK"))
    private Persona persona;

    @Column(name = "ID_PERFIL",nullable = false)
    private Long idPerfil;

    @Column(name = "IND_ACTIVO",nullable = false, length = 1)
    private String indActivo;


}
