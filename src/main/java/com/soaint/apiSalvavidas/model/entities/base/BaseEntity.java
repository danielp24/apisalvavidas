package com.soaint.apiSalvavidas.model.entities.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


@Data
@SuperBuilder
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"FECHA_CREACION", "FECHA_MODIFICACION"},
        allowGetters = true
)
public class BaseEntity implements Serializable {

    @Column(name = "UUID", nullable = false, updatable = false, length = 40)
    private String uuidjava = UUID.randomUUID().toString();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION", nullable = false, updatable = false)
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    @CreatedDate
    private Date fechaCreacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_MODIFICACION")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    @LastModifiedDate
    private Date fechaModificacion;

    @Column(name = "DIRECCION_IP", nullable = false,length = 50,updatable = false)
    private String direccionIP;

    @Column(name = "USUARIO_CREA", nullable = false,length = 20,updatable = false)
    private String usuarioCrea;

    @Column(name = "USUARIO_MODIFICA", length = 20)
    private String usuarioModifica;


    public BaseEntity() {
    }
}
