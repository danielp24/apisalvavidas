package com.soaint.apiSalvavidas.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soaint.apiSalvavidas.model.entities.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SuperBuilder
@Data
@NoArgsConstructor
@ToString
@Table(name = "PAIS")
@EqualsAndHashCode(callSuper = false, of = {"id"})
public class Pais extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "PAIS_GENERATOR")
    @Column(name = "ID_PAIS")
    @SequenceGenerator(
            name = "PAIS_GENERATOR",
            sequenceName = "PAIS_SEQ",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "CODIGO",nullable = false, length = 10)
    private String codigo;

    @Column(name = "DESCRIPCION",nullable = false, length = 100)
    private String descripcion;

}
