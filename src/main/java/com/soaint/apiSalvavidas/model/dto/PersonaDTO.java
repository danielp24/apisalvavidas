package com.soaint.apiSalvavidas.model.dto;

import com.soaint.apiSalvavidas.commons.domains.generic.AuditDTO;
import com.soaint.apiSalvavidas.commons.util.annotations.RequiredParameter;
import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class PersonaDTO {

    private Long id;
    private Long idPerfil;
    @RequiredPrimitiveParameter
    private String nombre1;
    private String nombre2;
    @RequiredPrimitiveParameter
    private String apellido1;
    private String apellido2;
    @RequiredPrimitiveParameter
    private Long idIdentificacion;
    @RequiredPrimitiveParameter
    private Long numeroIdentificacion;
    private Date fechaNacimiento;
    private Long idGenero;
    private Long idEdoCivil;
    private Long idRegimen;
    private String descRegimen;
    private Long idPerRep;

}
