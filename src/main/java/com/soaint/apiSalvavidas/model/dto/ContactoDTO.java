package com.soaint.apiSalvavidas.model.dto;

import com.soaint.apiSalvavidas.commons.util.annotations.RequiredParameter;
import lombok.*;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class ContactoDTO {

    private Long id;
    private String email;
    private Long idTipoTlf1;
    private String numeroTlf1;
    private Long idTipoTlf2;
    private String numeroTlf2;


}
