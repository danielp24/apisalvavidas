package com.soaint.apiSalvavidas.model.dto;

import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import com.soaint.apiSalvavidas.model.entities.Persona;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DatosAccesoDTO {

    private Long id;
    @RequiredPrimitiveParameter
    private String clave;
    private PersonaDTO persona;
    private Long idPerfil;
    @RequiredPrimitiveParameter
    private Long indActivo;
}
