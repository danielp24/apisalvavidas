package com.soaint.apiSalvavidas.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder


public class CrearUsuarioDTO {

    private Long id;
    private Long idPerfil;
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private Long idIdentificacion;
    private Long numeroIdentificacion;
    private Date fechaNacimiento;
    private Long idGenero;
    private Long idDepartamento;
    private Long idMunicipio;
    private String localidad;
    private String barrio;
    private String direccion;
    private String email;
    private Long idTipTel1;
    private String numeroTel1;
    private Long idTipTel2;
    private String numeroTel2;
}
