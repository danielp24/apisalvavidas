package com.soaint.apiSalvavidas.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class JwtDTO {

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private Date fechaInicial;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private Date fechaExpiracion;
    private String tipoToken;
    private String token;



}
