package com.soaint.apiSalvavidas.model.dto;

import lombok.*;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)

public class DatosDemograficosDTO {

    private Long id;
    private Integer codPais;
    private Integer codDepartamento;
    private Integer codMunicipio;
    private String localidad;
    private String barrio;
    private String direccion;
}
