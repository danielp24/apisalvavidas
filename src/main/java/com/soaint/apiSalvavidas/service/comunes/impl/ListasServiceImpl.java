package com.soaint.apiSalvavidas.service.comunes.impl;

import com.soaint.apiSalvavidas.commons.domains.response.comunes.GetCiudadMunicipioResponse;
import com.soaint.apiSalvavidas.commons.domains.response.comunes.GetDepartamentoResponse;
import com.soaint.apiSalvavidas.repository.comunes.impl.ComunesRepositoryFacade;
import com.soaint.apiSalvavidas.service.comunes.IListasService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ListasServiceImpl implements IListasService {

    private final ComunesRepositoryFacade facade;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public ListasServiceImpl(ComunesRepositoryFacade facade) {
        this.facade = facade;
    }

    @Override
    public Optional<Collection<GetDepartamentoResponse>> getDepartamento(String idPais) {

        return Optional.ofNullable(facade.getDepartamentos(idPais).get().stream()
                .map(depa -> modelMapper.map(depa, GetDepartamentoResponse.class))
                .collect(Collectors.toList()));
    }


    @Override
    public Optional<Collection<GetCiudadMunicipioResponse>> getCiudadMunicipio(String idDepartamento) {

        return Optional.ofNullable(facade.getCiudadMunicipio(idDepartamento).get().stream()
                .map(ciudad ->
                    modelMapper.map(ciudad, GetCiudadMunicipioResponse.class))
                .collect(Collectors.toList()));
    }

}

