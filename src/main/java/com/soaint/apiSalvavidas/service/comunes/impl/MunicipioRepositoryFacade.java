package com.soaint.apiSalvavidas.service.comunes.impl;

import com.soaint.apiSalvavidas.model.entities.CiudadMunicipio;

import java.util.Collection;
import java.util.Optional;

public interface MunicipioRepositoryFacade {

    Optional<CiudadMunicipio> registerCiudadMunicipio(final CiudadMunicipioDTORequest ciudadMunicipio);

    Optional<Collection<CiudadMunicipio>>findCiudadMunicipios();

    Optional<CiudadMunicipio> updaCiudadMunicipio(final CiudadMunicipioDTORequest ciudadMunicipios, Long id);

    Optional<CiudadMunicipio> getCiudadMunicipioById(final Long id);

    Optional<String> deleteCiudadMunicipio(final Long id);

}
