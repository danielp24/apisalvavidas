package com.soaint.apiSalvavidas.service.comunes;

import com.soaint.apiSalvavidas.commons.domains.response.comunes.GetCiudadMunicipioResponse;
import com.soaint.apiSalvavidas.commons.domains.response.comunes.GetDepartamentoResponse;

import java.util.Collection;
import java.util.Optional;

public interface IListasService {

    Optional<Collection<GetDepartamentoResponse>> getDepartamento(String idPais);

    Optional<Collection<GetCiudadMunicipioResponse>> getCiudadMunicipio(String idDepartamento);



}
