package com.soaint.apiSalvavidas.service.persona.impl;

import com.soaint.apiSalvavidas.commons.converter.PersonaConverter;
import com.soaint.apiSalvavidas.commons.domains.request.persona.CrearUsuarioRequest;
import com.soaint.apiSalvavidas.commons.domains.request.persona.LoginRequest;
import com.soaint.apiSalvavidas.commons.domains.response.personas.CrearUsuarioResponse;
import com.soaint.apiSalvavidas.commons.domains.response.personas.LoginResponse;
import com.soaint.apiSalvavidas.commons.util.exception.WebClientException;
import com.soaint.apiSalvavidas.commons.util.log.LogUtils;
import com.soaint.apiSalvavidas.model.dto.*;
import com.soaint.apiSalvavidas.model.entities.Persona;
import com.soaint.apiSalvavidas.repository.persona.impl.PersonaRepositoryFacade;
import com.soaint.apiSalvavidas.service.persona.IPersonaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Base64;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private ModelMapper modelMapper;

    @PersistenceContext
    EntityManager em;

    private final PersonaRepositoryFacade repository;

    @Autowired
    public PersonaServiceImpl(PersonaRepositoryFacade repository) {
        this.repository = repository;
    }



    @Override
    public Optional<Collection<PersonaDTO>> findPersonas() throws WebClientException {

        return Optional.ofNullable(repository.findPersonas().get().stream()
                .map(persona -> modelMapper.map(persona, PersonaDTO.class))
                .collect(Collectors.toList()));
    }

    @Override
    public Optional<PersonaDTO> updatePersona(PersonaDTO persona, Long id) throws WebClientException {
        return Optional.ofNullable(modelMapper.map(
                repository.updatePersona(
                    modelMapper.map(persona,Persona.class), id)
                ,PersonaDTO.class)
        );
    }

    @Override
    public Optional<String> detelePersona(Long id) throws WebClientException {
        Optional<String> ms = repository.deletePersona(id);
        return Optional.ofNullable(ms.get());
    }

    @Override
    public Optional<PersonaDTO> getPersonaById(Long id) throws WebClientException {
        Optional<Persona> persona = repository.getPersonaById(id);
        return Optional.ofNullable(persona.isPresent() ? modelMapper.map(persona.get(), PersonaDTO.class) : new PersonaDTO());
    }

    @Override
    public Optional<Collection<DatosAccesoDTO>> getDatosAcceso() throws WebClientException {

        return Optional.ofNullable(repository.getDatosAcceso().get().stream()
                .map(data -> modelMapper.map(data, DatosAccesoDTO.class))
                .collect(Collectors.toList()));
    }


    @Override
    public Optional<CrearUsuarioResponse> crearUsuario(CrearUsuarioRequest user) throws WebClientException {
            Optional<Persona> personaEntity = Optional.of( new Persona());
            PersonaConverter.crearUsuarioDtoToEntity(modelMapper,user,personaEntity);
            return Optional.ofNullable(
                        modelMapper.map(
                                repository.crearUsuario(personaEntity.get()).get()
                                ,CrearUsuarioResponse.class
                        )
                    );
    }


    @Override
    public LoginResponse login(LoginRequest loginDTO) throws WebClientException{

        Long idPersona = 0L;
        Long idPerfil = 0L;
        String password = "";
        String passwordB64 = "";
        LoginResponse response = new LoginResponse();

        try {
            //codificamos la contraseña
            password = loginDTO.getContraseña();
            passwordB64 = Base64.getEncoder().encodeToString(password.getBytes());

            //Obtenemos la Lista de personas
            Optional<Collection<PersonaDTO>> persona = Optional.of(repository.findPersonas().get().stream()
                    .map(per -> modelMapper.map(per, PersonaDTO.class))
                    .collect(Collectors.toList()));

            if (persona.isPresent()) {
                Collection<PersonaDTO> listPersona = persona.get();
                for (PersonaDTO it : listPersona) {
                    if (it.getNumeroIdentificacion().equals(loginDTO.getNumeroIdentificacion()) && it.getIdIdentificacion().equals(loginDTO.getTipoIdentificacion())) {
                        idPersona = it.getId();
                        idPerfil = it.getIdPerfil();
                        response.setMessage("Ok");
                        break;
                    } else {
                        response.setMessage("Usuario No existe");
                    }
                }

                if (!response.getMessage().equals("Usuario No existe")) {
                    //Obtenemos lista de DatosAcceso
                    Optional<Collection<DatosAccesoDTO>> accesoDTOS = Optional.of(repository.getDatosAcceso().get().stream()
                            .map(data -> modelMapper.map(data, DatosAccesoDTO.class))
                            .collect(Collectors.toList()));

                    if (accesoDTOS.isPresent()) {
                        Collection<DatosAccesoDTO> listaDatosAc = accesoDTOS.get();

                        for (DatosAccesoDTO dto : listaDatosAc) {
                            if (dto.getIndActivo() == 1) {
                                if (dto.getPersona().getId().equals(idPersona)
                                        && dto.getIdPerfil().equals(idPerfil)) {
                                    if (dto.getClave().equals(passwordB64)) {

                                        response.setMessage("Logueado");
                                        break;
                                    } else {
                                        response.setMessage("Contraseña Incorrecta");
                                    }
                                } else {

                                    response.setMessage("Usuario y/o contraseña Incorrectos");
                                }
                            } else {
                                response.setMessage("Usuario Inactivo");
                            }
                        }
                    }
                }
            }
            return response;
        } catch (Exception e) {
            LogUtils.error(e);
            throw new WebClientException("1000",e.getMessage());
        }
    }
}
