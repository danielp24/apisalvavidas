package com.soaint.apiSalvavidas.service.persona;

import com.soaint.apiSalvavidas.commons.domains.request.persona.CrearUsuarioRequest;
import com.soaint.apiSalvavidas.commons.domains.request.persona.LoginRequest;
import com.soaint.apiSalvavidas.commons.domains.response.personas.CrearUsuarioResponse;
import com.soaint.apiSalvavidas.commons.domains.response.personas.LoginResponse;
import com.soaint.apiSalvavidas.commons.util.exception.WebClientException;
import com.soaint.apiSalvavidas.model.dto.*;
import org.springframework.transaction.annotation.Transactional;


import java.util.Collection;
import java.util.Optional;

@Transactional
public interface IPersonaService {

    Optional<Collection<PersonaDTO>> findPersonas() throws WebClientException;

    Optional<PersonaDTO> getPersonaById(final Long id) throws WebClientException;

    Optional<PersonaDTO> updatePersona(final PersonaDTO persona, final Long id) throws WebClientException;

    Optional<String> detelePersona(final Long id) throws WebClientException;

    Optional<Collection<DatosAccesoDTO>> getDatosAcceso () throws WebClientException;

    Optional<CrearUsuarioResponse> crearUsuario (final CrearUsuarioRequest user) throws WebClientException;

    LoginResponse login (LoginRequest loginDTO) throws WebClientException;

}
