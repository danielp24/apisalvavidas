package com.soaint.apiSalvavidas.configuration;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2

public class SwaggerConfig {
    /**
     * Swagger Doc
     * @return
     */
    @Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("SALVAVIDAS-SERVICES")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.soaint.apiSalvavidas"))
                .paths(PathSelectors.any())
                .build()
                //.securitySchemes(Lists.newArrayList(apiKey()))
                //.securityContexts(Lists.newArrayList(securityContext()))
                .apiInfo(getApiInfo());
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    @Bean
    SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
                new SecurityReference("JWT", authorizationScopes));
    }

    /**
     * Aplication Info
     * @return
     */
    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "SALVAVIDAS REST API",
                "Esta API provee una capa para interactuar directamente con los servicios de negocio e interactuar con otras APIS",
                "1.0.0",
                "Rest",
                new Contact("SOAINT","http://soaint.com/","soaint@email.com"),
                Calendar.getInstance().get(Calendar.YEAR)+" - Soaint",
                "http://soaint.com/",
                Collections.emptyList()
        );
    }
}

