package com.soaint.apiSalvavidas.commons.domains.generic;

import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class AuditDTO implements Serializable {

    private Date fechaCreacion = new Date(); //Insert
    private Date fechaModificacion;// Update
    @RequiredPrimitiveParameter
    private String direccionIP;//Insert
    @RequiredPrimitiveParameter
    private String usuarioCrea;//Insert
    private String usuarioModifica;// Update

}

