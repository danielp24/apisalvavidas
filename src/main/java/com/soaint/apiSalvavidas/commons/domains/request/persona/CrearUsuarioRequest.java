package com.soaint.apiSalvavidas.commons.domains.request.persona;

import com.soaint.apiSalvavidas.commons.domains.generic.AuditDTO;
import com.soaint.apiSalvavidas.commons.util.annotations.RequiredParameter;
import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import com.soaint.apiSalvavidas.model.dto.ContactoDTO;
import com.soaint.apiSalvavidas.model.dto.DatosAccesoDTO;
import com.soaint.apiSalvavidas.model.dto.DatosDemograficosDTO;
import com.soaint.apiSalvavidas.model.dto.PersonaDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class CrearUsuarioRequest  {

    @RequiredParameter
    private AuditDTO headers;

    @RequiredParameter
    private PersonaDTO persona;

    @RequiredParameter
    private DatosAccesoDTO usuario;

    @RequiredParameter
    private List<DatosDemograficosDTO> datosDemograficos;

    @RequiredParameter
    private ContactoDTO contacto;

}
