package com.soaint.apiSalvavidas.commons.domains.response.comunes;

import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetDepartamentoResponse {

    @RequiredPrimitiveParameter
    private Integer idDepartamento;
    @RequiredPrimitiveParameter
    private String direccionIP;
    @RequiredPrimitiveParameter
    private Date fechaCreacion;
    @RequiredPrimitiveParameter
    private Date fechaModificacion;
    @RequiredPrimitiveParameter
    private String usuarioCrea;
    @RequiredPrimitiveParameter
    private String usuarioModifica;
    @RequiredPrimitiveParameter
    private String uuid;
    @RequiredPrimitiveParameter
    private String codigoDepartamento;
    @RequiredPrimitiveParameter
    private String descripcion;
    @RequiredPrimitiveParameter
    private Integer idPais;

}
