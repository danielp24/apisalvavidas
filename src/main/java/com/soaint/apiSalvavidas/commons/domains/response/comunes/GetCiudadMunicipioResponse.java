package com.soaint.apiSalvavidas.commons.domains.response.comunes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetCiudadMunicipioResponse {

    private Integer idCiudadMunicipio;
    private String descripcion;

}
