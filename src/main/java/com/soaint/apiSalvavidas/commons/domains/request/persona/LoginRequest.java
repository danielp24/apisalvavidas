package com.soaint.apiSalvavidas.commons.domains.request.persona;

import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest implements Serializable {

    @RequiredPrimitiveParameter
    private Long tipoIdentificacion;
    @RequiredPrimitiveParameter
    private Long numeroIdentificacion;
    @RequiredPrimitiveParameter
    private String contraseña;

}
