package com.soaint.apiSalvavidas.commons.domains.response.personas;

import com.soaint.apiSalvavidas.commons.domains.generic.AuditDTO;
import com.soaint.apiSalvavidas.model.dto.ContactoDTO;
import com.soaint.apiSalvavidas.model.dto.DatosDemograficosDTO;
import com.soaint.apiSalvavidas.model.dto.PersonaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class CrearUsuarioResponse extends AuditDTO {

    private AuditDTO headers;

    private PersonaDTO persona;

    private List<DatosDemograficosDTO> datosDemograficos;

    private ContactoDTO contacto;

}
