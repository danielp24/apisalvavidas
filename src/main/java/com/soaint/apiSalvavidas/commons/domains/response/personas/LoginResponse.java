package com.soaint.apiSalvavidas.commons.domains.response.personas;

import com.soaint.apiSalvavidas.commons.util.annotations.RequiredPrimitiveParameter;
import com.soaint.apiSalvavidas.model.dto.JwtDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse extends JwtDTO {

    private String message;

}
