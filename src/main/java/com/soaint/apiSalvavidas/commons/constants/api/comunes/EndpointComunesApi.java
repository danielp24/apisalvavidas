package com.soaint.apiSalvavidas.commons.constants.api.comunes;

public interface EndpointComunesApi {

    //Base de comunes
    String BASE_COMUNES = "/comunes/";

    //Base de operaciones
    String GET_DEPARTAMENTOS = "getDepartamentos/{id}";
    String GET_CIUDADMUNICIPIOS = "getCiudadMunicipio/{id}";
}
