package com.soaint.apiSalvavidas.commons.constants.api.persona;

public interface EndpointPersonaApi {

    //Base de Persona
    String BASE_PERSONA = "/persona/";

    String FIND_PERSONAS = "getPersonas";
    String FIND_PERSONAS_BY_ID = "{id}";
    String UPDATE_PERSONAS_BY_ID = "{id}";
    String DELETE_PERSONA = "{id}";
    String GET_DATOS_ACCESO = "getDataAccess";
    String CREATE_USUARIO = "crearUsuario";
    String LOGIN_USUARIO = "loginUsuario";
}
