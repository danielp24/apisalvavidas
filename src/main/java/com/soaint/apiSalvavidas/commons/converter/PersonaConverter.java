package com.soaint.apiSalvavidas.commons.converter;

import com.soaint.apiSalvavidas.commons.domains.request.persona.CrearUsuarioRequest;
import com.soaint.apiSalvavidas.model.entities.DatosDemograficos;
import com.soaint.apiSalvavidas.model.entities.Persona;
import org.modelmapper.ModelMapper;

import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PersonaConverter {


    public static void crearUsuarioDtoToEntity (ModelMapper modelMapper, CrearUsuarioRequest user, Optional<Persona> personaEntity){

        //Mapeando objeto Persona
        modelMapper.map(user.getPersona(),personaEntity.get());

        //Mapeando objeto Contacto
        modelMapper.map(user.getContacto(),personaEntity.get().getContacto());

        //Encode clave
        user.getUsuario().setClave(Base64.getEncoder().encodeToString(user.getUsuario().getClave().getBytes()));
        //Mapeando Usuario
        modelMapper.map(user.getUsuario(),personaEntity.get().getDatosAcceso());

        //Mapeando Perfil
        if(user.getPersona().getIdPerfil() != null){
            personaEntity.get().getDatosAcceso().setIdPerfil(user.getPersona().getIdPerfil());
        }

        //Mapeando Lista de Datos Demograficos
        Optional<List<DatosDemograficos>> listaDatosDem = Optional.of(user.getDatosDemograficos().stream().map(
                data -> modelMapper.map(data,DatosDemograficos.class)
        ).collect(Collectors.toList()));
        listaDatosDem.get().forEach( data -> modelMapper.map(user.getHeaders(),data));
        if(listaDatosDem.isPresent())
            personaEntity.get().getDatosDemograficos().addAll(listaDatosDem.get());

        //Mapeando Headers auditoria Para cada Objeto
        modelMapper.map(user.getHeaders(),personaEntity.get().getContacto());
        modelMapper.map(user.getHeaders(),personaEntity.get());
        modelMapper.map(user.getHeaders(),personaEntity.get().getDatosAcceso());


    }

}
