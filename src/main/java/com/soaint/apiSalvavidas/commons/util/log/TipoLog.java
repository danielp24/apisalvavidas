package com.soaint.apiSalvavidas.commons.util.log;

public enum TipoLog {
    DEBUG, ERROR, FATAL, INFO, WARNING
}