package com.soaint.apiSalvavidas.web.api.rest.personas;

import com.soaint.apiSalvavidas.commons.domains.request.persona.CrearUsuarioRequest;
import com.soaint.apiSalvavidas.commons.domains.request.persona.LoginRequest;
import com.soaint.apiSalvavidas.model.dto.ContactoDTO;
import com.soaint.apiSalvavidas.model.dto.CrearUsuarioDTO;
import com.soaint.apiSalvavidas.model.dto.DatosDemograficosDTO;
import com.soaint.apiSalvavidas.model.dto.PersonaDTO;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IPersonaApiController {


    ResponseEntity findPersonas(HttpServletRequest request);

    //ResponseEntity findPersonById(final Long id, HttpServletRequest request);

    ResponseEntity updatePersonaById(final PersonaDTO persona, Long id, HttpServletRequest request);

    ResponseEntity deletePersonaById(final Long id, HttpServletRequest request);

    ResponseEntity getDatosAcceso(HttpServletRequest request);

    ResponseEntity crearUsuario (final CrearUsuarioRequest user, HttpServletRequest request);

    ResponseEntity login (final LoginRequest loginDTO, HttpServletRequest request);

}
