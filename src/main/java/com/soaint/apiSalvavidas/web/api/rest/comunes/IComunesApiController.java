package com.soaint.apiSalvavidas.web.api.rest.comunes;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IComunesApiController {

    ResponseEntity getDepartamentos(String idPais,HttpServletRequest request);

    ResponseEntity getCiudadMunicipio (String idDepartamento, HttpServletRequest request);
}
