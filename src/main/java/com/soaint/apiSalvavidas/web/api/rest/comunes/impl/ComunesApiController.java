package com.soaint.apiSalvavidas.web.api.rest.comunes.impl;

import com.soaint.apiSalvavidas.commons.constants.api.base.EndPointBaseApi;
import com.soaint.apiSalvavidas.commons.constants.api.comunes.EndpointComunesApi;
import com.soaint.apiSalvavidas.commons.domains.response.builder.ResponseBuilder;
import com.soaint.apiSalvavidas.commons.domains.response.comunes.GetCiudadMunicipioResponse;
import com.soaint.apiSalvavidas.commons.domains.response.comunes.GetDepartamentoResponse;
import com.soaint.apiSalvavidas.commons.util.exception.ValidationUtils;
import com.soaint.apiSalvavidas.commons.util.exception.WebClientException;
import com.soaint.apiSalvavidas.commons.util.log.LogUtils;
import com.soaint.apiSalvavidas.service.comunes.IListasService;
import com.soaint.apiSalvavidas.web.api.rest.comunes.IComunesApiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(value = EndPointBaseApi.BASE+ EndpointComunesApi.BASE_COMUNES)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class ComunesApiController implements IComunesApiController {


    @Autowired
    private final IListasService iListasService;

    public ComunesApiController(IListasService iListasService) {
        this.iListasService = iListasService;
    }

    @GetMapping(EndpointComunesApi.GET_DEPARTAMENTOS)
    public ResponseEntity getDepartamentos(@PathVariable("id") String id,HttpServletRequest request)  {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyString(id);
            Optional<Collection<GetDepartamentoResponse>> depto = iListasService.getDepartamento(id);
            return response
                    .withStatus(HttpStatus.OK)
                    .withResponse(depto)
                    .buildResponse();
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus("1000")
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }


    }

    @GetMapping(EndpointComunesApi.GET_CIUDADMUNICIPIOS)
    public ResponseEntity getCiudadMunicipio(@PathVariable("id") String id, HttpServletRequest request) {
        LogUtils.error("lelgo = "+id);
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyString(id);
            Optional<Collection<GetCiudadMunicipioResponse>> ciudadMunicipio = iListasService.getCiudadMunicipio(id);
            if(ciudadMunicipio.isPresent()
                    &&ciudadMunicipio.get().size()>0){
                return response
                        .withResponse(ciudadMunicipio.isPresent() ? ciudadMunicipio.get() : null)
                        .withStatus(ciudadMunicipio.isPresent() ? HttpStatus.CREATED : HttpStatus.OK)
                        .withMessage("Lista de Ciudad/Municipio filtrado por = " + id + " Consultado exitosamente")
                        .buildResponse();
            } else {
                throw new WebClientException("1000", "No se han retornado elementos en la lista");
            }

        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus("1000")
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }

    }
}


