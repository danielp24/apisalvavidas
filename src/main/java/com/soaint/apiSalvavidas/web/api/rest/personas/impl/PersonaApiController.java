package com.soaint.apiSalvavidas.web.api.rest.personas.impl;

import com.soaint.apiSalvavidas.commons.constants.api.base.EndPointBaseApi;
import com.soaint.apiSalvavidas.commons.constants.api.persona.EndpointPersonaApi;
import com.soaint.apiSalvavidas.commons.domains.request.persona.CrearUsuarioRequest;
import com.soaint.apiSalvavidas.commons.domains.request.persona.LoginRequest;
import com.soaint.apiSalvavidas.commons.domains.response.personas.LoginResponse;
import com.soaint.apiSalvavidas.model.dto.DatosAccesoDTO;
import com.soaint.apiSalvavidas.model.dto.PersonaDTO;
import com.soaint.apiSalvavidas.commons.domains.response.builder.ResponseBuilder;
import com.soaint.apiSalvavidas.commons.domains.response.personas.CrearUsuarioResponse;
import com.soaint.apiSalvavidas.commons.enums.TransactionState;
import com.soaint.apiSalvavidas.commons.util.exception.ValidationUtils;
import com.soaint.apiSalvavidas.commons.util.exception.WebClientException;
import com.soaint.apiSalvavidas.commons.util.log.LogUtils;
import com.soaint.apiSalvavidas.service.persona.IPersonaService;
import com.soaint.apiSalvavidas.web.api.rest.personas.IPersonaApiController;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = EndPointBaseApi.BASE+EndpointPersonaApi.BASE_PERSONA)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class PersonaApiController implements IPersonaApiController {

    private final IPersonaService personaService;

    @Value("${security.jwt.secret}")
    public String SECRET;

    @Autowired
    public PersonaApiController(IPersonaService personaService) {
        this.personaService = personaService;
    }


    @GetMapping(EndpointPersonaApi.FIND_PERSONAS)
    public ResponseEntity findPersonas(HttpServletRequest request) {

        return ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.OK)
                .withResponse(personaService.findPersonas())
                .buildResponse();
    }

    /*@GetMapping(EndpointPersonaApi.FIND_PERSONAS_BY_ID)
    public ResponseEntity findPersonById(@PathVariable final Long id, HttpServletRequest request) {

        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            ValidationUtils.validateNullEmptyObject(persona);
            Optional<PersonaDTO> personaCreated = personaService.getPersonaById(id);
            response.withStatus(personaCreated.isPresent() ? HttpStatus.CREATED : HttpStatus.OK)
                    .withBusinessStatus("200")
                    .withResponse(personaCreated.isPresent() ? personaCreated.get() : new PersonaDTO())
                    .buildResponse();


            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(persona)
                    .withMessage("El Documento " + id + " Existe ")
                    .buildResponse();

        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus("1000")
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }

    }*/


    @PutMapping(EndpointPersonaApi.UPDATE_PERSONAS_BY_ID)
    public ResponseEntity updatePersonaById(@RequestBody final PersonaDTO persona,
                                            @PathVariable final Long id,
                                            HttpServletRequest request) {

        Optional<PersonaDTO> response = personaService.updatePersona(persona, id);

       /* if (response.get().getId() == null) {

            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.NOT_FOUND)
                    .withResponse(response)
                    .withMessage("El Documento " + id + " no Existe ")
                    .buildResponse();
        }*/

        return ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.OK)
                .withResponse(response)
                .buildResponse();
    }

    @DeleteMapping(EndpointPersonaApi.DELETE_PERSONA)
    public ResponseEntity deletePersonaById(@PathVariable final Long id, HttpServletRequest request) {

        return ResponseBuilder.newBuilder()
                .withResponse(personaService.detelePersona(id))
                .withMessage("Funciona!")
                .withStatus(HttpStatus.OK)
                .withTransactionState(TransactionState.OK)
                .buildResponse();
    }

    @GetMapping(EndpointPersonaApi.GET_DATOS_ACCESO)
    public ResponseEntity getDatosAcceso(HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            Optional<Collection<DatosAccesoDTO>> dataAccess = personaService.getDatosAcceso();
            response.withStatus(dataAccess.isPresent() ? HttpStatus.CREATED : HttpStatus.OK)
                    .withBusinessStatus("200")
                    .withResponse(dataAccess.isPresent() ? dataAccess : new DatosAccesoDTO())
                    .buildResponse();
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus("1000")
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }


    @PostMapping(EndpointPersonaApi.CREATE_USUARIO)
    public ResponseEntity crearUsuario (@RequestBody CrearUsuarioRequest user, HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyObject(user);
            Optional<CrearUsuarioResponse> createUser = personaService.crearUsuario(user);
            if(createUser.isPresent()) {
                response.withStatus(createUser.get().getPersona().getId() != null ? HttpStatus.CREATED : HttpStatus.OK)
                        .withBusinessStatus("200")
                        .withResponse(createUser)
                        .buildResponse();
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus("1000")
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @PostMapping(value = EndpointPersonaApi.LOGIN_USUARIO)
    public ResponseEntity login(@RequestBody LoginRequest login, HttpServletRequest request) throws WebClientException {
        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            ValidationUtils.validateNullEmptyObject(login);
            LoginResponse responseLogin = personaService.login(login);
            if (responseLogin.getMessage().equals("Logueado")) {
                getContextJWT(login,responseLogin);
                response.withStatus(HttpStatus.OK)
                        .withBusinessStatus("200")
                        .withMessage("Logueado")
                        .withResponse(responseLogin)
                        .buildResponse();
            } else {
                if (responseLogin.getMessage().equals("Usuario No existe")) {
                    response.withStatus(HttpStatus.BAD_REQUEST)
                            .withBusinessStatus("400")
                            .withMessage("Usuario No existe")
                            .withResponse(responseLogin)
                            .buildResponse();
                } else {
                    if (responseLogin.getMessage().equals("Contraseña Incorrecta")) {
                        response.withStatus(HttpStatus.BAD_REQUEST)
                                .withBusinessStatus("400")
                                .withMessage("Contraseña Incorrecta")
                                .withResponse(responseLogin)
                                .buildResponse();
                    } else {
                        if (responseLogin.getMessage().equals("Usuario y/o contraseña Incorrectos")) {
                            response.withStatus(HttpStatus.BAD_REQUEST)
                                    .withBusinessStatus("400")
                                    .withMessage("Usuario y/o contraseña Incorrectos")
                                    .withResponse(responseLogin)
                                    .buildResponse();
                        } else {
                                if (responseLogin.getMessage().equals("Usuario Inactivo")) {
                                response.withStatus(HttpStatus.BAD_REQUEST)
                                        .withBusinessStatus("400")
                                        .withMessage("Usuario Inactivo")
                                        .withResponse(responseLogin)
                                        .buildResponse();
                            }
                        }
                    }
                }
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus("1000")
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .withTransactionState(TransactionState.OK)
                    .buildResponse();
        }

    }

    private void getContextJWT(LoginRequest login, LoginResponse responseLogin) {

        final Instant now = Instant.now();

        String token = Jwts
                .builder()
                .setId("salvavidasJWT")
                .setSubject(login.getNumeroIdentificacion().toString())
                .claim("authorities",
                        "ROLE_USER")
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(1, ChronoUnit.DAYS)))
                .signWith(SignatureAlgorithm.HS512,
                        SECRET.getBytes()).compact();
        responseLogin.setToken("Bearer " + token);
        responseLogin.setFechaInicial(Date.from(now));
        responseLogin.setFechaExpiracion(Date.from(now.plus(1, ChronoUnit.DAYS)));
        responseLogin.setTipoToken("Bearer");
    }

}
