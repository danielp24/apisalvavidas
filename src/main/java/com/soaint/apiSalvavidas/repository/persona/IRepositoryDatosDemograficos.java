package com.soaint.apiSalvavidas.repository.persona;

import com.soaint.apiSalvavidas.model.entities.DatosDemograficos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRepositoryDatosDemograficos extends JpaRepository<DatosDemograficos, Long> {
}
