package com.soaint.apiSalvavidas.repository.persona;

import com.soaint.apiSalvavidas.model.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY,isolation = Isolation.READ_COMMITTED)
public interface  IPersonaRepository extends JpaRepository<Persona, Long> {

    @Query("select p from Persona p where p.id = :id")
    Persona findPersonaByIdPersona(@Param("id") final Long id);





}
