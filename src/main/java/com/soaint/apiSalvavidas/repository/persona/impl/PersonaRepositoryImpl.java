package com.soaint.apiSalvavidas.repository.persona.impl;

import com.soaint.apiSalvavidas.commons.util.exception.WebClientException;
import com.soaint.apiSalvavidas.model.dto.ContactoDTO;
import com.soaint.apiSalvavidas.model.dto.DatosDemograficosDTO;
import com.soaint.apiSalvavidas.model.dto.PersonaDTO;
import com.soaint.apiSalvavidas.commons.util.log.LogUtils;
import com.soaint.apiSalvavidas.model.entities.*;
import com.soaint.apiSalvavidas.repository.persona.IDatosAccesoRepository;
import com.soaint.apiSalvavidas.repository.persona.IPersonaRepository;
import com.soaint.apiSalvavidas.repository.persona.IRepositoryContacto;
import com.soaint.apiSalvavidas.repository.persona.IRepositoryDatosDemograficos;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

@Component
@Log4j2
public class PersonaRepositoryImpl implements PersonaRepositoryFacade {

    private final IPersonaRepository repository;
    private final IDatosAccesoRepository repositoryDatos;

    @Autowired
    public PersonaRepositoryImpl(IPersonaRepository repository, IDatosAccesoRepository repositoryDatos) throws UnknownHostException {
        this.repository = repository;
        this.repositoryDatos = repositoryDatos;
    }


    @Override
    public Optional<Collection<Persona>> findPersonas() {
        LogUtils.info("PersonaRepositoryImpl. consultando personas.");

        Collection<Persona> personas = new ArrayList<>(repository.findAll());
        return Optional.of(personas);
    }

    @Override
    public Optional<Persona> updatePersona(Persona persona, Long id) {

        LogUtils.info("PersonaRepositoryImpl. actualizando persona por el id : " + id);

        Persona upPersona = repository.findPersonaByIdPersona(id);

        if (upPersona == null) {
            LogUtils.info("PersonaRepositoryImpl. No se encontro la persona por el id : " + id);
            return Optional.of(Persona.builder().build());
        }

        upPersona.setNombre1(persona.getNombre1());
        upPersona.setApellido1(persona.getApellido1());
        upPersona.setIdIdentificacion(1L);
        upPersona.setNumeroIdentificacion(persona.getNumeroIdentificacion());

        return Optional.of(repository.save(upPersona));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Persona> getPersonaById(Long id) {

        LogUtils.info("PersonaRepositoryImpl. consultando persona por el id : " + id);

        return Optional.ofNullable(repository.findById(id).orElse(null));
    }

    @Override
    public Optional<String> deletePersona(Long id) {

        Optional<Persona> persona = getPersonaById(id);

        if (persona.isPresent()) {
            repository.deleteById(id);
            return Optional.ofNullable("Eliminado satisfactoriamente");
        } else {
            return Optional.ofNullable("Registro no encontrado");
        }

    }

    @Override
    public Optional<Collection<DatosAcceso>> getDatosAcceso() {
        LogUtils.info("PersonaRepositoryImpl. consultando datos acceso.");
        Collection<DatosAcceso> datosAccesos = new ArrayList<>(repositoryDatos.findAll());
        return Optional.of(datosAccesos);
    }

    @Override
    public Optional<Persona> crearUsuario(Persona persona) throws WebClientException {
        LogUtils.info(">>>>>>>>>> Creando usuario en fachada");
            Optional<Persona> resPersona = Optional.ofNullable(this.repository.save(persona));
        return Optional.of(resPersona.get());
    }
}
