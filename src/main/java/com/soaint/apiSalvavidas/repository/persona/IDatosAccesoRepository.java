package com.soaint.apiSalvavidas.repository.persona;

import com.soaint.apiSalvavidas.model.entities.DatosAcceso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDatosAccesoRepository extends JpaRepository<DatosAcceso, Long> {
}
