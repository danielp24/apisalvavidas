package com.soaint.apiSalvavidas.repository.persona;

import com.soaint.apiSalvavidas.model.entities.Contacto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRepositoryContacto extends JpaRepository<Contacto, Long> {
}
