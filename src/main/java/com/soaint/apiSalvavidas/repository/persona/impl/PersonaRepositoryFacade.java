package com.soaint.apiSalvavidas.repository.persona.impl;

import com.soaint.apiSalvavidas.commons.util.exception.WebClientException;
import com.soaint.apiSalvavidas.model.dto.PersonaDTO;
import com.soaint.apiSalvavidas.model.entities.DatosAcceso;
import com.soaint.apiSalvavidas.model.entities.Persona;

import java.util.Collection;
import java.util.Optional;

public interface PersonaRepositoryFacade {


    Optional<Collection<Persona>> findPersonas() throws WebClientException;

    Optional<Persona> updatePersona(final Persona persona, Long id) throws WebClientException;

    Optional<Persona> getPersonaById(final Long id) throws WebClientException;

    Optional<String> deletePersona(final Long id) throws WebClientException;

    Optional<Collection<DatosAcceso>> getDatosAcceso() throws WebClientException;

    Optional<Persona> crearUsuario(final Persona persona) throws WebClientException;


}
