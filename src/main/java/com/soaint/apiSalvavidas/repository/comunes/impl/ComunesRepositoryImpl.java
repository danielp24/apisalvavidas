package com.soaint.apiSalvavidas.repository.comunes.impl;

import com.soaint.apiSalvavidas.model.entities.CiudadMunicipio;
import com.soaint.apiSalvavidas.model.entities.Departamento;
import com.soaint.apiSalvavidas.repository.comunes.IDepartamentoRepository;
import com.soaint.apiSalvavidas.repository.comunes.IMunicipioRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
@Log4j2
public class ComunesRepositoryImpl implements ComunesRepositoryFacade {

    private final IDepartamentoRepository departamentoRepository;
    private final IMunicipioRepository municipioRepository;
    @Autowired
    public ComunesRepositoryImpl(IDepartamentoRepository repository, IDepartamentoRepository departamentoRepository, IMunicipioRepository municipioRepository) {
        this.departamentoRepository = departamentoRepository;
        this.municipioRepository = municipioRepository;
    }


    @Override
    public Optional<Collection<Departamento>> getDepartamentos(String idPais) {
        Collection<Departamento> departa = new ArrayList<>(departamentoRepository.findAll());
        return Optional.of(departa);
    }
    @Override
    public Optional<Collection<CiudadMunicipio>> getCiudadMunicipio(String idDepartamento) {
        Collection<CiudadMunicipio> ciudadMunicipio= new ArrayList<>(municipioRepository.findAll());
       /* CiudadMunicipio ciudadMunicipio1 = repository.findPersonaByIdPersona(id);*/
        return Optional.of(ciudadMunicipio);
    }
/* consutar por id ciudadMunicipios
    @Override
    @Transactional(readOnly = true)
    public Optional<CiudadMunicipio> getCiudadMunicipioById(Long id) {

        log.info("ComunesRepositoryImpl. consultando persona por el id : " + id);

        return Optional.ofNullable(repository.findById(id).orElse(null));
    }*/
}
