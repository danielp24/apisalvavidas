package com.soaint.apiSalvavidas.repository.comunes.impl;

import com.soaint.apiSalvavidas.model.entities.CiudadMunicipio;
import com.soaint.apiSalvavidas.model.entities.Departamento;

import java.util.Collection;
import java.util.Optional;

public interface ComunesRepositoryFacade {

    Optional<Collection<Departamento>> getDepartamentos(String idPais);

    Optional<Collection<CiudadMunicipio>> getCiudadMunicipio(String idDepartamento);
}
