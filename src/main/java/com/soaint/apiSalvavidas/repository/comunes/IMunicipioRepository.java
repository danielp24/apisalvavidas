package com.soaint.apiSalvavidas.repository.comunes;

import com.soaint.apiSalvavidas.model.entities.CiudadMunicipio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMunicipioRepository extends JpaRepository<CiudadMunicipio, String> {
/*
    @Query("select p from ciudad_municipio p where p.id_departamento = :id_departamento")
    ciudad_municipio findCiudadMunicipioByIdDepartamento(@Param("id_departamento") final Long id);*/
}
