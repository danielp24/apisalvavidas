package com.soaint.apiSalvavidas.repository.comunes;

import com.soaint.apiSalvavidas.model.entities.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDepartamentoRepository extends JpaRepository<Departamento, String> {


}
